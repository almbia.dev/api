async function getCharacters() {
    try {
      
      const response = await fetch("https://jsonplaceholder.typicode.com/users");
  
      const characterList = await response.json();

      for (const oneCharacter of characterList) {
       
        console.log("Nom:", oneCharacter.name);
  
        const address = oneCharacter.address;
        
      console.log("Rue:", address.street);

      console.log("Code postal:", address.zipcode);

      console.log("Ville:", address.city);

      }
    } catch (error) {
      console.error("Erreur lors de la récupération des personnages:", error);
    }
  }
  
  getCharacters();

  
  
